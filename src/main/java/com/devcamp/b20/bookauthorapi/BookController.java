package com.devcamp.b20.bookauthorapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.b20.bookauthorapi.models.Author;
import com.devcamp.b20.bookauthorapi.models.Book;

@RestController
@RequestMapping("/api")
public class BookController {
    @CrossOrigin
    @GetMapping("/books")

    public ArrayList<Book> getBooks(){
        Author author1 = new Author("Jonathan Swift", "Swift@gmail.com", 'N');
        Author author2 = new Author("Đỗ Đức Hiếu", "Hieu@gmail.com", 'N');
        Author author3 = new Author("Phan Thái dịch", "dich@gmail.com", 'N');

        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);

        Book book1 = new Book("Gulliver du kí", author1, 68900, 1);
        Book book2 = new Book("Hoa tuylip đen", author2, 70000, 1);
        Book book3 = new Book("Túp lều bác Tom", author3, 69000, 1);

        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);

        ArrayList<Book> arrListBook = new ArrayList<Book>();

        arrListBook.add(book1);
        arrListBook.add(book2);
        arrListBook.add(book3);

        return arrListBook;

    }
}
